*****************************************************************************************
*                                     Polydraw.s                                        * 
*****************************************************************************************
* SECTION TEXT
* assembler directive
  opt d+        put in labeles for debugging
  bra main      dont' try to execute the includes

* all these files are to be include here
  include equates.s   all the constants
	include bss_00.s    variables locations
	include data_00.s   mainly standar palette colours
	include systm_00.s  a lot of housekeeping routins
	include core_00.s   the meat
*****************************************************************************************
* heres the main control program
main
  bsr   alloc_mem   allocate memory for screens etc
  bsr   copr_list   set up the copper lists
  bsr   blit_alloc  take over the blitter
  bsr   colr_set    set up the standard palette
  bsr   wrt_phys_tbl  look-up table for fast screen access

* The program cycles here; screen buffering is used
blit_loop:
* first draw a triangle
  bsr   drw1_shw2     draw on screen 1, display screen 2
  lea   my_coords,a0  the vertices defined in the triangle
  move.l  a0,coords_lst here's where to find them
  move.w  #2,colour   coloured red
  move.w  #3,no_in    3 sides to a triangle
  bsr     poly_fill   draw the outline and fill it red

* then an inverted triangle
  bsr     drw2_shw1   draw on screen 2, display screen 1
  lea     my_inv_coords,a0  the vertices
  move.l  a0,coords_list  here they are
  move.w  #12,colour    coloured green
  bsr     poly_fill     draw the outline and fill it green
  bra blit_loop         repeat the cycle



* The coordinates of the two triangles

my_coords dc.w 100,160,150,70,190,140,100,160
my_inv_coords dc.w 100,80,160,160,170,90,100,80

  END
