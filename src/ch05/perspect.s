*
* perspect.s
*

*SECTION TEXT
  opt   d+        labels for debugging
  bra   main      dont execute the includes
  include core_02.s core subroutines
  include systm_00.s

main  bsr   set_up    allocate memory etc

* Transfer data from the data file to variables locations:
* first the edge numbers and colours
  move.w  my_npoly,d7 no of polygons?
  beq     main        if none, quit
  move.w  d7,npoly    or becomes
  subq.w  #1,d7       the counter
  move.w  d7,d0       save it
  lea     my_nedges,a0  source
  lea     snedges,a1    destination
  lea     my_colour,a2    source
  lea     col_lst,a3      destination
loop0 move.w  (a0)+,(a1)+   transfer edge nos
      move.w  (a2)+,(a3)0   transfer colours
      dbra    d0,loop0

* second the edge list and coordinates
    move.w  d7,d0 restore count
    lea     my_nedges,a6
    clr     d1
    clr     d2

loop1 add.w (a6),d1
      add.w (a6)+,d2
      addq  #1,d2     last one repeated each time
      dbra  d0,loop1  = total no of vertices
      subq  #1,d2     the counter
      lea   my_edglst,a0  source
      lea   sedglst,a1    destination
loop2 move.w  (a0)+,(a1)+  pass it
      dbra    d2,loop2
      move.w  d1,vncoords
      subq    #1,d1
      lea     vcoordsx,a1
      lea     my_datax,a0
      lea     vcoordsy,a3
      lea     my_datay,a2
      lea     vcoordsz,a5
      lea     my_dataz,a4

loop3 move.w  (a0)+,(a1)+
      move.w  (a2)+,(a3)+
      move.w  (a4)+,(a5)+
      dbra    d1,loop3

* the clip form boundaries
      move.w  my_xmin,clp_xmin  ready
      move.w  my_xmax,clp_xmax  for
      move.w  my_ymin,clp_ymin  clipping
      move.w  my_ymax,clp_ymax  clipping
* Calculate the perspective view and draw it
bit_loop:
  bsr drw_shw2
  bsr perspective
  bsr polydraw
  bsr drw2_shw1
pers_loop
  bra pers_loop forever

*SECTION DATA
  include data_01.s
  include data_02.s
*SECTION BSS
  include bss_02.s
  END
