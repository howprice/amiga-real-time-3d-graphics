* illhide.s
* A program illustrating illumination and hidden surface removal
*

*SECTION TEXT
  opt   d+
  bra   main
  include systm_00.s
  include core_04.s       illumination, hidden surface removal

main  bsr init            set up memory and new palette, etc

* transfer all the data from my lists to progress lists
      bsr transfer
* place it in the world frame
      move.w  #0,Oox      on the ground
      move.w  #100,Ooz    100 in front
      clr.w   Ooy         dead centre
* Initialise angles for rotation
      clr.w   otheta
      move.w  #50,ophi    tilt it forward
      clr.w   ogamma
* Initalize screens
      clr.w   screenflag  0=screen 1 draw, 1=screen 2 draw

* Start the rotation about the xw axis
loop5 move.w  #360,d7     a cycle
loop4 move.w  d7,otheta   next theta
      move.w  d7,-(sp)    save the angle
      tst.w   screenflag  screen 1 orscreen2?
      beq     screen_1    draw on screen 1, display screen2
      bsr     drw2_shw1   draw on screen 2, display screen1
      clr.w   screenflag  and set the flag for next time
      bra     screen_2
screen_1:
      bsr     drw1_shw2   draw on 1,display 2
      move.w  #1,screenflag and set the flag for next time
screen_2:
      bsr     otranw        object-to-world transform

* pass on the new coords
      move.w  oncoords,d7
      move.w  d7,vncoords
      subq.w  #1,d7
      lea     wcoordsx,a0
      lea     wcoordsy,a1
      lea     wcoordsz,a2
      lea     vcoordsx,a3
      lea     vcoordsy,a4
      lea     vcoordsz,a5
loop6 move.w  (a0)+,(a3)+
      move.w  (a1)+,(a4)+
      move.w  (a2)+,(a5)0
      dbra    d7,loop6

* Test for visibility and lightning
    bsr       illuminate      if it is viisble find the shade

* Complete the drawing
    bsr       perspective     perspective
    bsr       polydraw        finish the picture
    move.w    (sp)+,d7
    sub.w     #10,d7          decrement in 10 degree stpes
    bgt       loop4
    bra       loop5

* SECTION DATA
    include   data_01.s
    include   data_03.s
    include   data_04.s
* SECTION BSS
    include   bss_04.s
END
