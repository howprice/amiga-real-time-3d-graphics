*****************************************************************************************
*                                System_01.s                                            *
*****************************************************************************************
init_vars:
* set up the screens
 bsr  init
* Set up the view point
 move.w #100,oposx
 clr.w oposy
 clr.w oposz 
* and the clip frame
 move.w #50,clp_xmin
 move.w #270,clp_xmax
 move.w #30,clp_ymin
 move.w #170,clp_ymax
* Set up view frame base vectors
*1. iv
 lea    iv,a0 								align view frame axes
 move.w #$4000,(a0)+
 move.w #0,(a0)+
 move.w #0,(a0)
*2. jv
 lea   jv,a0                  with the world frame
 clr.w (a0)+
 move.w #$4000,(a0)+
 clr.w (a0)
*3.kv
 lea    kv,a0
 move.w #0,(a0)+
 clr.w (a0)+
 move.w #$4000,(a0)

flg_init: 
* Initialize flags and other variables
 clr.w speed                  start at rest
 clr.w screenflag             0=screen 1 draw, 1=screen 2 draw
 clr.w viewflag
* Move the view point to -300 on the view frame z axis
 lea   persmatx,a0
 move.w #300,d0
 move.w d0,(a0)
 move.w d0,10(a0)
 move.w d0,30(a0)
 rts
swap_scn:
  tst.w screenflag  screen 1 or screen2?
  beq   screen_1    draw on screen 1, display screen2
  bsr   drw2_shw1   draw on screen 2, display screen1
  clr.w screenflag  and set the flag for next time
  bra   screen_2

screen_1:
  bsr     drw1_shw2   drar on 1, display 2
  move.w  #1,screenflag and set the flag for next time 
screen_2:
  rts
 
    
