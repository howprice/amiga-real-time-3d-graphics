* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* wrld_scn.s
* A multi-object scene
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* A world scene consisting of various types of graphics primitives
* in notion. The viewer is free to *fly* to any location. At any 
* position a patch consisting of 4*4 "tiles" is visible.
* Joystick controls Yaw and pitch. F1 and F2 controll roll
* Don't held down keys as keyboard buffer is not cleared.

* SECTION TEXT
    opt   d+
    bra   main
    include systm_01.s
    include core_07.s

main:
* Initalize the system.
    bsr   init_vars     initialize view transform
    bsr   flg_init      initialize  flags

loop:
* Read input and make adjustments.
    bsr   swp_scn       swap the screens
    bsr   dircosines    regenerate view matrix
    bsr   joy_read      see which direction to move
    bsr   in_key        update the speed
    bsr   adj_vel       adjust the velocity

* Draw the scene
    bsr   scne_drw      everything to complete the picture

* Draw the next frame 
    bra   loop

*SECTIOM DATA
    include data_00.s
    include data_06.s
    include data_07.s
    include data_08.s
*SECTION BSS
    include bss_07.s

END
